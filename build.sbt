name := """improvisation"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.8"

lazy val root = project.in(file(".")).enablePlugins(PlayScala)

resolvers += "Typesafe repository releases" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
	"org.reactivemongo" %% "play2-reactivemongo" % "0.11.14"
)
