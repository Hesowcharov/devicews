package models.device

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class Device private(val name: String, 
    val enabled: Boolean, 
    val params: Map[String, JsValue])

object Device {
  //TODO: creating devices by db
  
  implicit val deviceWrites: OWrites[Device] = (
      (__ \ "name").write[String] and
      (__ \ "enabled").write[Boolean] and
      (__ \ "options").write[Map[String, JsValue]])(unlift(Device.unapply _))
  
  implicit val deviceReads: Reads[Device] = (
      (__ \ "name").read(Reads.minLength[String](1)) and
      (__ \ "enabled").read[Boolean] and
      (__ \ "options").read[Map[String, JsValue]])(Device.apply _)
      
}