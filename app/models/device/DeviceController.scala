package models.device

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import play.api.libs.json._

import play.modules.reactivemongo.{MongoController}
import reactivemongo.play.json.collection.JSONCollection
import reactivemongo.play.json._
import reactivemongo.api.commands.WriteResult

trait DeviceController { this: MongoController =>
  
  type FutureResult[T] = Either[FailedOperation, T]
  
  private[models] def futureCollection: Future[JSONCollection] = {
    for {
      db <- database
    } yield db.collection[JSONCollection]("devices")
  }
  
  def getDevice(name: String): Future[FutureResult[Device]] = {
    val futOptDevice = for {
      col <- futureCollection
      optDevice <- col.find(Json.obj{ "name" -> name }).one[Device]
    } yield optDevice
    futOptDevice.map { optDevice =>
      optDevice match {
        case Some(device) => (Right(device))
        case None => 
          Left(FailedOperation("Device with such name doesn't exist."))
      }
    }
  }
  
  def allDevices(): Future[List[Device]] = {
    for {
      col <- futureCollection
      devices <- col.find( Json.obj() ).cursor[Device]().collect[List]()
    } yield  devices
  }
  
  def specificQuery(selector: JsObject, projection: JsObject): 
      Future[List[JsObject]] = {
    
    for {
      col <- futureCollection
      jss <- col.find(selector, projection)
                .cursor[JsObject]()
                .collect[List]()
    } yield jss
  }
  
  def insertDevice(device: Device): Future[FutureResult[WriteResult]] = {
    val name = device.name
    getDevice(name).flatMap { failOrDevice =>
      failOrDevice match {
        case Right(_) => 
          Future.successful(
              Left(FailedOperation(s"Device with name \'$name\' already exist."))
              )
        case Left(_) => 
          for {
            col <- futureCollection
            writeResult <- col.insert(device)
          } yield Right(writeResult) 
      }
    }
  }
  
  def removeDevice(name: String): Future[FutureResult[WriteResult]] = {
    getDevice(name).flatMap { failOrDevice =>
      failOrDevice match {
        case Left(opResult) => Future.successful(Left(opResult))
        case Right(_) => 
          for {
            col <- futureCollection
            res <- col.remove(Json.obj("name" -> name), firstMatchOnly = true)
          } yield Right(res)
      }
    }
  }
  
  def updateDevice(device: Device): Future[FutureResult[WriteResult]] = {
    getDevice(device.name).flatMap { failOrDevice =>
      failOrDevice match {
        case Left(fail) => Future.successful(Left(fail))
        case Right(foundDevice) => for {
          col <- futureCollection
          res <- col.update(foundDevice, device)
        } yield Right(res)
      }
    }
  }
} 