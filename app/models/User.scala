package models

case class User(name: String, password: String, role: Role)
