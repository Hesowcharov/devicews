package controllers

//scala and play
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._

import scala.concurrent.Future

// Reactive Mongo imports
import play.modules.reactivemongo.{ // ReactiveMongo Play2 plugin
  MongoController,
  ReactiveMongoApi,
  ReactiveMongoComponents
}

// BSON-JSON conversions/collection - JSON serialization pack
import reactivemongo.play.json._
import javax.inject.Inject

//Models
import models.device._

class Application @Inject() (val reactiveMongoApi: ReactiveMongoApi)
    extends Controller
    with DeviceController
    with MongoController
    with ReactiveMongoComponents {

  def index = Action {
    val apiInfo = Json.obj(
      "Ver" -> "0.1",
      "API" -> Json.arr(
        Json.obj("All devices" -> "GET on /api/devices"),
        Json.obj("Details of a device" -> "GET on /api/devices/{name}"),
        Json.obj("Create device" ->
          ("POST on /api/devices with structure {" +
            "name : {string}, enabled : {boolean}, options : {object with pairs} }")
          ),
        Json.obj("Update device" ->
          ("PUT on /api/devices/{name} with structure {" +
            "name : {string}, enabled : {boolean}, options : {object with pairs} }")
          ),
        Json.obj("Delete device" -> "DELETE on /api/devices/{name}")
      )
    )
    Ok(apiInfo)
  }

  def getAboutDevices() = Action.async {
    val selector = Json.obj("name" -> Json.obj("$exists" -> true))
    val projection = Json.obj("name" -> 1, "_id" -> 0)
    val deviceNames = specificQuery(selector, projection)
    deviceNames.map { ds =>
      Ok(Json.toJson(ds))
    }
  }

  def getDeviceBy(name: String) = Action.async {
    getDevice(name).map { failOrDevice =>
      failOrDevice match {
        case Right(dev) => Ok(Json.toJson(dev))
        case Left(fail) => NotFound(fail.errMessage)
      }
    }
  }

  def addDevice() = Action.async (parse.json) { request =>
    val json = request.body
    json.validate[Device] match {
      case JsError(_) => Future.successful(BadRequest("Invalid device json."))
      case JsSuccess(dev,_) => 
        insertDevice(dev).map { failOrResult => 
          failOrResult match {
            case Right(_) => 
              Created withHeaders ("Location" -> s"/devices/${dev.name}")
            case Left(fail) => BadRequest(fail.errMessage)
          }
        }
    }
  }

  def updateDeviceBy(name: String) = Action.async(parse.json) { request =>
    request.body.validate[Device] match {
      case JsError(_) => Future.successful( BadRequest("Invalid device json.") )
      case JsSuccess(dev, _) =>
        val updatedName = dev.name
        if (name != updatedName) {
          Future ( BadRequest("Device json have incorrect name.") )
        } else {
          updateDevice(dev).map { failOrResult =>
            failOrResult match {
              case Left(fail) => NotFound(fail.errMessage)
              case Right(_) => Ok("Device was updated")
            }
          }
        }
    }
  }
  
  def deleteDeviceBy(name: String) = Action.async {
    removeDevice(name).map { failOrResult =>
      failOrResult match {
        case Left(fail) => NotFound(fail.errMessage)
        case Right(_) => Ok(s"Device \'$name\' was deleted.")
      }
    }
  }

}
