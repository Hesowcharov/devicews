# README #
###This project is simple REST API web service. It is just improvisation to try such technologies as play! 2 framework (with using Scala) and Reactive Mongo. ###
#What is need#
* MongoDB (>= 2.6)
* sbt (>= 0.13.12)
# How To Run #
Clone this repository and changing directory:
```
git clone https://Hesowcharov@bitbucket.org/Hesowcharov/devicews.git
cd device-controller
```
Run sbt to resolve dependency and go to interactive console. Then perform task "run":
```
sbt
run
```
Do GET request to localhost:9000/api (or just open this link in your browser) to get api info.